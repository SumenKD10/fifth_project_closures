function cacheFunction(callBackFunction) {
    let cacheValues = {};
    if (!(typeof callBackFunction === 'function')) {
        throw new Error("Error is here.");
    }
    function invokingFunction(...argumentsGiven) {
        console.log(cacheValues);
        if (!(argumentsGiven in cacheValues)) {
            cacheValues[argumentsGiven] = callBackFunction(...argumentsGiven);
            return cacheValues[argumentsGiven];
        } else {
            return cacheValues[argumentsGiven];
        }
    }
    return invokingFunction;
}

module.exports = cacheFunction;