function limitFunctionCall(callBackFunction, limitGiven) {
    let invokedTimes = 0;
    if (!(typeof limitGiven === 'number') || !(typeof callBackFunction === 'function')) {
        throw new Error("Error is Here");
    }
    function invokingFunction(...argumentsGiven) {
        if (invokedTimes < limitGiven) {
            invokedTimes++;
            return callBackFunction(...argumentsGiven);
        } else {
            return null;
        }
    }
    return invokingFunction;
}

module.exports = limitFunctionCall;