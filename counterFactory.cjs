function counterFactory() {
    let counter = 0;
    function increment() {
        return ++counter;
    }
    function decrement() {
        return --counter;
    }
    let endObject = {
        increment: increment,
        decrement: decrement
    }
    return endObject;
}

module.exports = counterFactory;