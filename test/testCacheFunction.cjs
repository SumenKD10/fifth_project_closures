let cacheFunction = require('../cacheFunction.cjs');

function callBackFunction(...argumentsGiven) {
    return "Callback Function Invoked.";
}

try {
    let result = cacheFunction(callBackFunction);
    console.log(result(1, 2, 3));
    console.log(result(3, 2, 1));
    console.log(result("Good Evening", 2));
    console.log(result("Good Night"));
    result("Good Morning");
    result("Good Afternoon");
    console.log(result("Good Evening", 2));
    console.log(result("Good Night"));
} catch (errorCatched) {
    console.error(errorCatched);
}