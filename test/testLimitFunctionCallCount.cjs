let limitFunctionCall = require('../limitFunctionCallCount.cjs');

function callBackFunction(...argumentsGiven) {
    console.log("Invoked CallBack Function");
}
let limitGiven = 4;
try {
    let result = limitFunctionCall(callBackFunction, limitGiven);
    result(1, 5);
    result();
    result();
    result();
    result();
    result();
    result();
    result();
} catch (errorWeGot) {
    console.error(errorWeGot);
}
